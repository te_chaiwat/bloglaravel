@extends('main')


@section('content')
<div class="row-fluid">
    <p>
        <h1>Hellow Blog Laravel </h1>
    </p>
    <h1>{{ 'title ='.$data['title'] .' | content='. $data['content'] }} </h1>
    <p><b>{{ URL::current() }}</b></p>
    <p>
        {!! $data['content'] !!}
    </p>
</div>
@endsection